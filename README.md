# README


This program simulates a roof truss. Given the loads at each point, this gives the minimum beam thickness.

project.pdf contains the handout from the professor. 

To run the simulation, execute projectScript.m