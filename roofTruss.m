% Clayton Salinger Ketner
% University of Southern California
% AME 404
% November 19, 2012
%
% Final project - roof truss simulation
% INPUTS
%   a = horizontal distance between adjacent nodes along the bottom [m]
%   h = total height of the truss [m]
%   b = vector of the external forces on the truss [N]
% OUTPUTS
%   X = vector of the forces in each beam element of the truss [N]
function X = roofTruss(a, h, b)
A = zeros(24,24);

t1 = atan(3*a/h);
t12 = atan(h/(3*a));
t10 = pi/2 - t12;
t6 = atan(2*a/(2/3*h));
t4 = atan(a/(2/3*h));
t5 = pi/2 - t4;
t8 = t12;


A(1,1) = -cos(t12); A(1,2) = -1; A(1,22) = -1; % Ax
A(2,1) = sin(t12); A(2,23) = -1; % Ay
A(3,1) = cos(t12); A(3,4) = -cos(t12); A(3,5) = -sin(t10); % Bx
A(4,1) = -sin(t12); A(4,3) = -1; A(4,4) = sin(t12); A(4,5) = -cos(t10); % By
A(5,2) = 1; A(5,6) = -1; % Cx
A(6,3) = 1; % Cy
A(7,10) = -sin(t6); A(7,4) = sin(t6); A(7,9) = -sin(t4); % Dx
A(8,7) = -1; A(8,10) = cos(t6); A(8,4) = -cos(t6); A(8,9) = -cos(t4); % Dy
A(9,8) = -1; A(9,6) = 1; A(9,5) = cos(t8); % Ex
A(10,7) = 1; A(10,5) = sin(t8); % Ey

A(11,10) = sin(t1); A(11,12) = -sin(t1); % Fx
A(12,11) = -1; A(12,10) = -cos(t1); A(12,12) = -cos(t1); % Fy
A(13,14) = -1; A(13,8) = 1; A(13,9) = cos(t5); A(13,13) = -cos(t5); % Gx
A(14,11) = 1; A(14,9) = sin(t5); A(14,13) = sin(t5); % Gy

A(15,12) = sin(t6); A(15,16) = -sin(t6); A(15,13) = sin(t4); % Hx
A(16,15) = -1; A(16,12) = cos(t6); A(16,16) = -cos(t6); A(16,13) = -cos(t4); % Hy
A(17,14) = 1; A(17,18) = -1; A(17,17) = -cos(t8); % Ix
A(18,15) = 1; A(18,17) = sin(t8); % Iy
A(19,20) = -cos(t12); A(19,17) = sin(t10); A(19,16) = cos(t12); % Jx
A(20,16) = sin(t12); A(20,20) = -sin(t12); A(20,19) = -1; A(20,17) = -cos(t10); % Jy
A(21,21) = -1; A(21,18) = 1; % Kx
A(22,19) = 1; % Ky
A(23,20) = cos(t12); A(23,21) = 1; % Lx
A(24,20) = sin(t12); A(24,24) = -1; % Ly


X = myLUDecompWithPivot(A, b);