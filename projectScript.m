% Clayton Salinger Ketner
% University of Southern California
% AME 404
% November 27, 2012
%
% Final Project
% Script file
clear
clc

% Conditions from #2 in the handout
a = 5;
h = 8;
b = zeros(24,1);

% Set b values
b(8) = 1000; % F_Dy
b(12) = 1000; % F_Fy
b(16) = 1000; % F_Hy
b(10) = 1000; % F_Ey
b(14) = 1000; % F_Gy

roofTrussAnalyzer(a, h, b);






