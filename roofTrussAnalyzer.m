% Clayton Salinger Ketner
% University of Southern California
% AME 404
% December 4, 2012
%
% Final Project
% Roof truss analyzer
% Evaluates the stresses and prints out the results for the given roof
% truss
% INPUTS
%   a = horizontal distance between adjacent nodes along the bottom [m]
%   h = total height of the truss [m]
%   b = vector of the external forces on the truss [N]
function X = roofTrussAnalyzer(a, h, b)
fprintf('Using down and to the right as positive: \n\n');

X = roofTruss(a, h, b);

fprintf(['a = ' num2str(a) ' [m], h = ' num2str(h) ' [m] \n\n']);

bLabels = {'A' 'B' 'C' 'D' 'E' 'F' 'G' 'H' 'I' 'J' 'K' 'L'};
xLabels = {'BA' 'AC' 'BC' 'DB' 'BE' 'CE' 'DE' 'EG' 'DG' 'FD' 'FG' 'FH' ...
           'HG' 'GI' 'HI' 'HJ' 'IJ' 'IK' 'JK' 'JL' 'KL' 'R_Ax' 'R_Ay' 'R_Ly'};
       
padding = numLength(max(abs(b))); % Number length of max magnitude

fprintf('B: \t\t\t\t X:\n');
fprintf('-----\t\t\t----- \n');
for (ii = 1:1:24)
    fprintf('F_');
    fprintf(bLabels{ceil(ii/2)});
    if (mod(ii,2) == 0)
        fprintf('y');
    else
        fprintf('x');
    end
    
    fprintf([': ' num2str(b(ii))]);
    
    for (jj = 1:1:padding - numLength(b(ii)) - 1)
        fprintf(' ');
    end
    
    fprintf(['\t\t' xLabels{ii} ': ' num2str(X(ii)) '\n']);
end

fprintf('\nTesting: \n');
t12 = atan(h/(3*a));
% Moment about A
y1 = a*tan(t12);
y2 = 2*a*tan(t12);

R_Ly = 1/(6*a)*(-b(6)*a - b(10)*2*a - b(14)*3*a - b(18)*4*a - b(22)*5*a ...
                - b(24)*6*a - b(4)*a - b(3)*y1 - b(8)*2*a - b(7)*y2 ...
                - b(12)*3*a - b(11)*h - b(16)*4*a - b(15)*y2 - b(20)*5*a - b(19)*y1);

% Sum of forces in the y
R_Ay = -sum(b(2:2:24)) - R_Ly;

% Sum of forces in the x
R_Ax = -sum(b(1:2:23));

fprintf(['\tR_Ax: ' num2str(R_Ax) '\n']);
fprintf(['\tR_Ay: ' num2str(R_Ay) '\n']);
fprintf(['\tR_Ly: ' num2str(R_Ly) '\n']);

fprintf('\nPart 3: \n');
fprintf('Materials (using A242 structural steel): \n');
yieldStr = 340000000; % N/m^2
E = 200000000000; % N/m^2
rho = 7850; % kg/m^3
fprintf(['\tYield strength = ' num2str(yieldStr/1000000) ' [MPa] \n']);
fprintf(['\tYoungs modulus = ' num2str(E/1000000000) ' [GPa] \n']);
fprintf(['\tDensity = ' num2str(rho) ' [kg/m^3] \n']);

% Find the minimum radius of the beams
% Tension is positive
minR_tension(1) = sqrt(max(X(1:21))/(yieldStr*pi)); % Index = Factor of safety
minR_tension(2) = sqrt(2*max(X(1:21))/(yieldStr*pi));

L1 = sqrt(a^2 + (h/3)^2);
L2 = sqrt(a^2 + (2/3*h)^2);
L = [L1 a h/3 L1 L1 a 2/3*h a L2 L1 h L1 L2 a 2/3*h L1 L1 a h/3 L1 a]';

minR_compression = zeros(1,2); % Index = Factor of safety
minR_test = minR_compression; % Index = Factor of safety

for (ii = 1:1:21)
    if (X(ii) < 0)
        minR_test(1) = (4*abs(X(ii))*L(ii)^2/(pi^3*E))^(1/4);
        minR_test(2) = (4*abs(2*X(ii))*L(ii)^2/(pi^3*E))^(1/4);
        
        for (jj = 1:1:2)
            if (minR_test(jj) > minR_compression(jj))
                minR_compression(jj) = minR_test(jj);
            end
        end
    end
end

for (fos = 1:1:2)
    fprintf(['\n\tMinimum beam diameter: (FACTOR OF SAFETY = ' num2str(fos) ') \n']);
    fprintf(['\t\tTension: ' num2str(minR_tension(fos)*2) ' [m]\n']);
    fprintf(['\t\tCompression: ' num2str(minR_compression(fos)*2) ' [m]\n']);
    fprintf(['\t\tFinal: ' num2str(max(minR_tension(fos), minR_compression(fos))*2) ' [m] ']);
    
    % Say whether the final radius was due to tension/compression
    if (minR_tension(fos) > minR_compression(fos))
        fprintf('(due to tension) \n');
    else
        fprintf('(due to compression) \n');
    end
end