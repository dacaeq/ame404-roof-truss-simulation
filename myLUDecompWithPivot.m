% Clayton S Ketner
% University of Southern California
% AME 404
% October 15, 2012
%
% Lower/Upper tri decomposition with pivoting
% Decomposes A into LU and solves the systems LY = B; UX = Y
% INPUTS
%   A = n by n matrix
%   B = n by 1 matrix
% OUPUTS
%   X = n by 1 matrix - contains the solution to AX = B
function X = myLUDecompWithPivot(A, B)
n = size(A,1);

% Check inputs
if (size(A,2) ~= n || size(B,1) ~= n || size(B,2) ~= 1)
    error('Input A must be n by n, and B must be n by 1');
end

X = zeros(n,1);
Y = zeros(n,1);
C = zeros(1,n);
R = 1:n;

for (p = 1:1:n-1)
    % Find the pivot row for column p
    [~, j] = max(abs(A(p:n, p)));
    % Swap row p and j
    C = A(p,:);
    A(p,:) = A(j+p-1,:);
    A(j+p-1,:) = C;
    
    d = R(p);
    R(p) = R(j+p-1);
    R(j+p-1) = d;
    
    if (A(p,p) == 0)
        error('A is singular - there is no unique solution.');
    end
    
    % Calculate multiplier and place it in the subdiagonal of A
    for (k = p+1:1:n)
        mult = A(k,p) / A(p,p);
        A(k,p) = mult;
        A(k,p+1:n) = A(k,p+1:n) - mult*A(p,p+1:n);
    end
end

% Solve for Y
Y(1) = B(R(1));
for (k = 2:1:n)
    Y(k) = B(R(k)) - A(k, 1:k-1)*Y(1:k-1);
end

% Finally solve for X
X(n) = Y(n) / A(n,n);

for (k = n-1:-1:1)
    X(k) = (Y(k) - A(k,k+1:n)*X(k+1:n)) / A(k,k);
end




