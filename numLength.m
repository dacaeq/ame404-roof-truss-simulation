% Clayton Salinger Ketner
% November 27, 2012
%
% Returns the number of columns a number takes up when printed
function L = numLength(x)
L = 0;

if (x == 0)
    L = 1;
    return;
end

% Add count for negative sign
if (x < 0)
    L = L + 1;
end

while (abs(x) >= 1)
    x = x / 10;
    L = L + 1;
end